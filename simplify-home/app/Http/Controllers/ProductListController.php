<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductList;
use App\ProductListsItems;

class ProductListController extends Controller
{
    //create list
    public function setList(Request $request)
    {
        $list = new ProductList();

        try{
            $list->user_id = $request->user()->id;
            $list->titel = $request->input('titel');

            $list->save();
            $list->touch();

            // Create listitems
            if ($request->input('TotalItems') > 0) {
                for ($i = 0; $i < $request->input('TotalItems'); $i++) { 
                    $listItem = new ProductListsItems();
                    $listItem->list_id = $list->id;
                    $listItem->status = "open";
                    $listItem->count = $request->input('count' . (string)$i);
                    $listItem->product = $request->input('product' . (string)$i);
                    $listItem->save();
                }
                    
            }
            return $request->all();
           //return json_encode(array("" => ""));
        }
        catch (Exception $e){
            abort(412);
        }
    }

    public function getLists(Request $request){
        return json_encode(ProductList::where('user_id', '=', $request->user()->id)->get(), JSON_FORCE_OBJECT);
    }

    public function getList(Request $request, $titel){
        return json_encode(ProductListsItems::where('list_id', '=', ProductList::where('user_id', '=', $request->user()->id)->where('titel', '=', $titel)->first()->id)->get(), JSON_FORCE_OBJECT);
    }

    public function updateList(Request $request, $list_id){
        try {
            ProductList::where('user_id', '=', $request->user()->id)->where('id', '=', $list_id)->update([
                'titel' => $request->input('titel')
            ]);

            // Create listitems
            if ($request->input('TotalItems') > 0) {
                if (ProductListsItems::where('list_id', '=', $list_id)->get()) {
                    ProductListsItems::where('list_id', '=', $list_id)->delete();
                }
                for ($i = 0; $i < $request->input('TotalItems'); $i++) { 
                    $listItem = new ProductListsItems();
                    $listItem->list_id = $list_id;
                    $listItem->status = "open";
                    $listItem->count = $request->input('count' . (string)$i);
                    $listItem->product = $request->input('product' . (string)$i);
                    $listItem->save();
                }  
            }

            return json_encode($request, JSON_FORCE_OBJECT);
        }catch(Exception $e) {
            return $e;
        }
    }

    public function deleteList(Request $request, $list_id){
        try {
            if (ProductListsItems::where('list_id', '=', $list_id)->get()) {
                ProductListsItems::where('list_id', '=', $list_id)->delete();
            }
            ProductList::where('user_id', '=', $request->user()->id)->where('id', '=', $list_id)->delete();
            return json_encode($request, JSON_FORCE_OBJECT);
        }catch(Exception $e) {
            return $e;
        }
    }

    public function updatestatusItem(Request $request, $product){
        try {
            ProductListsItems::where('product', '=', $product)->update([
                'status' => $request->input('status')
            ]);
            return json_encode($request, JSON_FORCE_OBJECT);
        }catch(Exception $e) {
            return $e;
        }
    }
}
