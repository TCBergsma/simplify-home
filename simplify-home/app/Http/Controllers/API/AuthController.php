<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends ResponseController
{
    //create user
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|',
            'email' => 'required|string|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|string|min:8|same:password'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if ($user) {
            $success['token'] =  $user->createToken('token')->accessToken;
            $success['message'] = "Registration successfull..";
            $userAddToken = User::where('email', '=', $request->input('email'))->first();
            try{
                $userAddToken->api_token = $success['token'];
                $userAddToken->save();
            } catch (Exception $e) {
                abort(412);
            }
            return $this->sendResponse($success);
        } else {
            $error = "Sorry! Registration is not successfull.";
            return $this->sendError($error, 401);
        }
    }

    //login
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            $error = "Unauthorized";
            return $this->sendError($error, 401);
        }
        $user = $request->user();
        $success['token'] =  $user->createToken('token')->accessToken;
        $userAddToken = User::where('email', '=', $request->input('email'))->first();
            try{
                $userAddToken->api_token = $success['token'];
                $userAddToken->save();
            } catch (Exception $e) {
                abort(412);
            }
        return $this->sendResponse($success);
    }

    //logout
    public function logout(Request $request)
    {
        $isUser = $request->user()->token()->revoke();
        if ($isUser) {
            $success['message'] = "Successfully logged out.";
            return $this->sendResponse($success);
        } else {
            $error = "Something went wrong.";
            return $this->sendResponse($error);
        }
    }

    //getuser
    public function getUser(Request $request)
    {
        //$id = $request->user()->id;
        $user = $request->user();
        if ($user) {
            return $this->sendResponse($user);
        } else {
            $error = "user not found";
            return $this->sendResponse($error);
        }
    }

    //update user
    public function updateUser(Request $request){
        try {
            User::where('id', '=', $request->user()->id)->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
            ]);
   
            return json_encode($request, JSON_FORCE_OBJECT);
         }catch(Exception $e) {
               return $e;
         }
    }

    public function changePassword(Request $request)
    {
        // Check if current password is correct and passwords match
        $hashedPassword = User::where('id', '=', $request->user()->id)->first()->password;

        if (!Hash::check($request->password_current, $hashedPassword) && $request->password == $request->password_confirmation) {
            return response()->json([
                'errors' => [
                    'password_current' => ['Deze gegevens komen niet overeen.']
                ]
            ], 422);
        } else if (Hash::check($request->password_current, $hashedPassword) && $request->password != $request->password_confirmation) {
            return response()->json([
                'errors' => [
                    'password_confirmation' => ['Het nieuwe wachtwoord komt niet overeen.']
                ]
            ], 422);
        } else if (!Hash::check($request->password_current, $hashedPassword) && $request->password != $request->password_confirmation) {
            return response()->json([
                'errors' => [                    
                    'password_current' => ['Deze gegevens komen niet overeen.'],
                    'password_confirmation' => ['Het nieuwe wachtwoord komt niet overeen.']
                ]
            ], 422);
        }        

        // $this->validate($request, [
        //     'email' => 'required',
        //     'password_current' => 'required',
        //     'password' => 'required|string|min:8|confirmed'
        // ]);

        // Double check if the new passwords are the same
        // if (Hash::make($request->password) == Hash::make($request->password_confirmation)) {
        //     $request->user()->fill([
        //         'password' => Hash::make($request->password)
        //     ])->save();
        // };

        $request->user()->update([
            'password' => Hash::make($request->password)
        ]);

        // return json_encode($request, JSON_FORCE_OBJECT);
        return json_encode(array('' => ''));        
    }
}
