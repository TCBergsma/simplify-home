<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;

class DeviceController extends Controller
{
   //create device
   public function setDevice(Request $request)
   {
      $device = new Device();

      try{
         $device->user_id = $request->user()->id;
         $device->name = $request->input('name');
         $device->image_url = $request->input('image_url');
         $device->last_timer = 12;

         $device->save();
         return json_encode(array("" => ""));
      }
      catch (Execption $e){
         abort(412);
      }
      
   }

   public function getDevices(Request $request){
      return json_encode(Device::where('user_id', '=', $request->user()->id)->get(), JSON_FORCE_OBJECT);
   }

   public function getDevice(Request $request, $id){
      return json_encode(Device::where('user_id', '=', $request->user()->id)->where('id', '=', $id)->first(), JSON_FORCE_OBJECT);
   }

   public function deleteDevice(Request $request, $id){
      try {
         Device::where('user_id', '=', $request->user()->id)->where('id', '=', $id)->delete();

         return json_encode($request, JSON_FORCE_OBJECT);
      }catch(Exception $e) {
            return $e;
      }
   }

   public function updateDevice(Request $request, $id){
      try {
         Device::where('user_id', '=', $request->user()->id)->where('id', '=', $id)->update([
            'image_url' => $request->input('image_url'),
            'name' => $request->input('name'),
         ]);

         return json_encode($request, JSON_FORCE_OBJECT);
      }catch(Exception $e) {
            return $e;
      }
   }

   public function updateDeviceTimer(Request $request, $id){
      try {
         Device::where('user_id', '=', $request->user()->id)->where('id', '=', $id)->update([
            'last_timer' => $request->input('last_timer'),
         ]);

         return json_encode($request, JSON_FORCE_OBJECT);
      }catch(Exception $e) {
            return $e;
      }
   }
}
