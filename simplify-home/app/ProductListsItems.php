<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductListsItems extends Model
{
    protected $table = "product_lists_items";
}
