<?php

use Illuminate\Database\Seeder;
use Users;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Chris',
            'email' => 'christiaanbergsma@gmail.com',
            'password' => 'chris123456789'
        ]);
        DB::table('users')->insert([
            'name' => 'Marlène',
            'email' => 'ikhebeenleven@outlook.com',
            'password' => '1234567890'
        ]);
    }
}
