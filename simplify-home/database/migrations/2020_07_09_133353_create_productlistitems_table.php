<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductlistitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_lists_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('list_id');
            $table->string('status', 255);
            $table->string('count');
            $table->string('product', 255);
            $table->timestamps();
        });

        Schema::table('product_lists_items', function (Blueprint $table) {        
            $table->foreign('list_id')->references('id')->on('product_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_lists_items');
    }
}
