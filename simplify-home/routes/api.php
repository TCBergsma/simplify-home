<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::group(['middleware' => ['guest:api']], function () {
        Route::post('login', 'API\AuthController@login');
        Route::post('signup', 'API\AuthController@signup');
    });
    Route::group(['middleware' => 'auth:api'], function () {
        // Dit zijn de routes die je alleen kan benaderen als je ingelogd bent.
        Route::get('logout', 'API\AuthController@logout');
        Route::get('getuser', 'API\AuthController@getUser');
        Route::post('update-user', 'API\AuthController@updateUser');
        Route::post('reset-password', 'API\AuthController@changePassword');
        Route::post('add-device', 'DeviceController@setDevice');
        Route::get('get-device', 'DeviceController@getDevices');
        Route::get('get-device/{id}','DeviceController@getDevice');
        Route::post('update-device/{id}','DeviceController@updateDevice');
        Route::post('update-device-timer/{id}','DeviceController@updateDeviceTimer');
        Route::delete('delete-device/{id}', 'DeviceController@deleteDevice');
        
        Route::post('add-list', 'ProductListController@setList');
        Route::get('get-lists', 'ProductListController@getLists');
        Route::get('get-list/{titel}','ProductListController@getList');
        Route::post('update-list/{list_id}','ProductListController@updateList');
        Route::delete('delete-list/{list_id}','ProductListController@deleteList');
        Route::post('update-statusItem/{product}','ProductListController@updatestatusItem');
    });
});

