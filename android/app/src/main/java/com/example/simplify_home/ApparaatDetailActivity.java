package com.example.simplify_home;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.database.VolleySingleton;
import com.example.simplify_home.database.objects.Device;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ApparaatDetailActivity extends AppCompatActivity {
    private EditText mEditTextInput;
    private EditText mEditTextInputUur;
    private TextView mTextViewCountDown;
    private Button mButtonSet;
    private Button mButtonStartPause;
    private Button mButtonReset;
    private CountDownTimer mCountDownTimer;
    private boolean mTimerRunning;
    private long mStartTimeInMillis;
    private long mTimeLeftInMillis;
    private long mEndTime;
    private long millisInput;
    private Button mButtonEdit;
    private Button mButtonBack;
    private CircularImageView mapparaatImageDetail;
    private TextView mapparaatNameDetail;
    private NotificationManagerCompat notificationManagerCompat;
    private String apparaatNaam;
    private Integer incomingId;
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apparaat_detail);

        db = ((App) getApplicationContext()).getDb();

        mEditTextInput = findViewById(R.id.edit_text_input);
        mEditTextInputUur = findViewById(R.id.edit_text_input_uren);
        mTextViewCountDown = findViewById(R.id.text_view_countdown);
        mButtonSet = findViewById(R.id.button_set);
        mButtonStartPause = findViewById(R.id.button_start_pause);
        mButtonReset = findViewById(R.id.button_reset);
        mButtonEdit = findViewById(R.id.buttonEdit);
        mButtonBack = findViewById(R.id.buttonBackDetail);
        mapparaatImageDetail = findViewById(R.id.apparaatImageDetail);
        mapparaatNameDetail = findViewById(R.id.apparaatNaamDetail);
        notificationManagerCompat = NotificationManagerCompat.from(this);
        getIncomingIntent();

        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toHomepagina = new Intent(v.getContext(), HomeActivity.class);
                v.getContext().startActivity(toHomepagina);
            }
        });

        mButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((App)getApplication()).isConnected()) {
                    Intent toApparaatAanpassen = new Intent(v.getContext(), ApparaatAanpassenActivity.class);
                    toApparaatAanpassen.putExtra("id", getIncomingId());
                    ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(ApparaatDetailActivity.this, mapparaatImageDetail, "imageSlide");
                    v.getContext().startActivity(toApparaatAanpassen, activityOptionsCompat.toBundle());
                }else{
                    new AlertDialog.Builder(ApparaatDetailActivity.this)
                            .setTitle("Geen verbinding")
                            .setMessage("Oh nee! Het lijkt erop dat je geen verbinding met het internet hebt.\nMaak verbinding en probeer het dan nog een keer.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });

        mButtonSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputMin = mEditTextInput.getText().toString();
                String inputUur = mEditTextInputUur.getText().toString();
                if (inputMin.length() == 0 && inputUur.length() == 0) {
                    Toast.makeText(ApparaatDetailActivity.this, "Fields can't be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(inputMin.length() != 0){
                    millisInput = Long.parseLong(inputMin) * 60000;
                }
                if(inputUur.length() != 0){
                    millisInput += Long.parseLong(inputUur) * 3600000;
                }
                if (millisInput == 0) {
                    Toast.makeText(ApparaatDetailActivity.this, "Please enter a positive number", Toast.LENGTH_SHORT).show();
                    return;
                }
                setTime(millisInput);
                mEditTextInput.setText("");
                mEditTextInputUur.setText("");
            }
        });

        mButtonStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimerRunning) {
                    pauseTimer();
                } else {
                    startTimer();
                }
            }
        });
        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
            }
        });

    }

    public void sendNotification(){
        Notification notification = new NotificationCompat.Builder(this, App.CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_house_lightbulb_black_24dp)
                .setContentTitle("Timer is afgelopen")
                .setContentText("De timer van '" + apparaatNaam + "' is afgelopen")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .build();
        notificationManagerCompat.notify(1, notification);
    }

    private void getIncomingIntent(){
        if(getIntent().hasExtra("id")){
            Integer imageId = getIntent().getIntExtra("id", 1);
            changeImage(imageId);
            incomingId = imageId;
        }
    }

    private Integer getIncomingId(){
        if(getIntent().hasExtra("id")){
            Integer imageId = getIntent().getIntExtra("id", 1);
            return imageId;
        }
        return null;
    }

    private void changeImage(Integer imageId){
        JSONObject body = new JSONObject();
        try {
            body.put("id", imageId);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(((App)getApplication()).isConnected()) {
            Request request = VolleySingleton.getInstance(this).getDevice(imageId.toString(), body, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    ArrayList<String> namen = new ArrayList<>();
                    ArrayList<String> image = new ArrayList<>();
                    ArrayList<Integer> id = new ArrayList<>();
                    ArrayList<Integer> tijden = new ArrayList<>();

                    try {
                        JSONObject item = (JSONObject) response;
                        namen.add(item.getString("name"));
                        image.add(item.getString("image_url"));
                        id.add(item.getInt("id"));
                        tijden.add(item.getInt("last_timer"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (tijden.get(0) != 12) {
                        mTimeLeftInMillis = tijden.get(0);
                        mStartTimeInMillis = tijden.get(0) + 1;
                    }
                    updateCountDownText();
                    apparaatNaam = namen.get(0);
                    mapparaatNameDetail.setText(namen.get(0));
                    Context context = mapparaatImageDetail.getContext();
                    int drawAble = context.getResources().getIdentifier(image.get(0), "drawable", context.getPackageName());
                    mapparaatImageDetail.setImageResource(drawAble);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }
        else{
            ArrayList<String> namen = new ArrayList<>();
            ArrayList<String> image = new ArrayList<>();
            ArrayList<Integer> id = new ArrayList<>();
            ArrayList<Integer> tijden = new ArrayList<>();

            List<Device> list = db.deviceDAO().getDevice(imageId);
            for(Device device : list){
                namen.add(device.name);
                image.add(device.image_url);
                id.add(device.id);
                tijden.add(device.last_timer);
            }

            if (tijden.get(0) != null) {
                mTimeLeftInMillis = tijden.get(0);
                mStartTimeInMillis = tijden.get(0) + 1;
            }
            updateCountDownText();
            apparaatNaam = namen.get(0);
            mapparaatNameDetail.setText(namen.get(0));
            Context context = mapparaatImageDetail.getContext();
            int drawAble = context.getResources().getIdentifier(image.get(0), "drawable", context.getPackageName());
            mapparaatImageDetail.setImageResource(drawAble);
        }
    }

    private void setTime(long milliseconds) {
        mStartTimeInMillis = milliseconds;
        updateLastTimer(mStartTimeInMillis);
        resetTimer();
        closeKeyboard();
    }

    private void startTimer() {
        mEndTime = System.currentTimeMillis() + mTimeLeftInMillis;
        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }
            @Override
            public void onFinish() {
                mTimerRunning = false;
                sendNotification();
                updateWatchInterface();
            }
        }.start();
        mTimerRunning = true;
        updateWatchInterface();
    }

    private void pauseTimer() {
        mCountDownTimer.cancel();
        mTimerRunning = false;
        updateWatchInterface();
    }

    private void resetTimer() {
        mTimeLeftInMillis = mStartTimeInMillis;
        updateCountDownText();
        updateWatchInterface();
    }

    private void updateCountDownText() {
        int hours = (int) (mTimeLeftInMillis / 1000) / 3600;
        int minutes = (int) ((mTimeLeftInMillis / 1000) % 3600) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;
        String timeLeftFormatted;
        if (hours > 0) {
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%02d:%02d:%02d", hours, minutes, seconds);
        } else {
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%02d:%02d:%02d", hours, minutes, seconds);
        }
        mTextViewCountDown.setText(timeLeftFormatted);
    }

    private void updateWatchInterface() {
        if (mTimerRunning) {
            mEditTextInput.setVisibility(View.INVISIBLE);
            mEditTextInputUur.setVisibility((View.INVISIBLE));
            mButtonSet.setVisibility(View.INVISIBLE);
            mButtonReset.setVisibility(View.INVISIBLE);
            mButtonStartPause.setText("Pause");
        } else {
            mEditTextInput.setVisibility(View.VISIBLE);
            mEditTextInputUur.setVisibility(View.VISIBLE);
            mButtonSet.setVisibility(View.VISIBLE);
            mButtonStartPause.setText("Start");
            if (mTimeLeftInMillis < 1000) {
                mButtonStartPause.setVisibility(View.INVISIBLE);
            } else {
                mButtonStartPause.setVisibility(View.VISIBLE);
            }
            if (mTimeLeftInMillis < mStartTimeInMillis) {
                mButtonReset.setVisibility(View.VISIBLE);
            } else {
                mButtonReset.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("startTimeInMillis", mStartTimeInMillis);
        editor.putLong("millisLeft", mTimeLeftInMillis);
        editor.putBoolean("timerRunning", mTimerRunning);
        editor.putLong("endTime", mEndTime);
        editor.apply();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        mStartTimeInMillis = prefs.getLong("startTimeInMillis", 600000);
        mTimeLeftInMillis = prefs.getLong("millisLeft", mStartTimeInMillis);
        mTimerRunning = prefs.getBoolean("timerRunning", false);
        updateCountDownText();
        updateWatchInterface();
        if (mTimerRunning) {
            mEndTime = prefs.getLong("endTime", 0);
            mTimeLeftInMillis = mEndTime - System.currentTimeMillis();
            if (mTimeLeftInMillis < 0) {
                mTimeLeftInMillis = 0;
                mTimerRunning = false;
                updateCountDownText();
                updateWatchInterface();
            } else {
                startTimer();
            }
        }
    }

    private void updateLastTimer(Long time){
        JSONObject body = new JSONObject();
        try {
            body.put("last_timer", time);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(((App)getApplication()).isConnected()){
            Request request = VolleySingleton.getInstance(this).updateCountDownTimer(incomingId.toString(), body, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            db.deviceDAO().updateTimer(incomingId, time);
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }

    }
}