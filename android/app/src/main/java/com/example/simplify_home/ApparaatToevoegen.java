package com.example.simplify_home;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.database.VolleySingleton;
import com.example.simplify_home.database.objects.Device;
import com.google.android.material.textfield.TextInputEditText;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApparaatToevoegen extends AppCompatActivity {

    private TextInputEditText inputApparaatNaam;
    private Button mButtonBackAdd;
    private Button mButtonSave;
    private CircularImageView mCircularImageView;
    private String imageTag = "image";
    VolleySingleton volleySingleton;
    final private String BASEURL = "http://192.168.0.176:8000"; // M: http://192.168.178.9:8000 C: http://192.168.0.176:8000
    final private String ADDDEVICEURL = "/api/auth/add-device";
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apparaat_toevoegen);

        volleySingleton = VolleySingleton.getInstance(this.getApplicationContext());
        inputApparaatNaam = findViewById(R.id.inputApparaatNaam);
        mButtonBackAdd = findViewById(R.id.buttonBackAdd);
        mButtonSave = findViewById(R.id.buttonSave);
        mCircularImageView = findViewById(R.id.apparaatImageToevoegen);
        db = ((App) getApplicationContext()).getDb();

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveDevice();
            }
        });

        mButtonBackAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHomeActivity();
            }
        });
    }

    private void openHomeActivity(){
        Intent toHomeActivity = new Intent(this, HomeActivity.class);
        this.startActivity(toHomeActivity);
    }

    private void SaveDevice(){
        JSONObject body = new JSONObject();
        try {
            body.put("name", inputApparaatNaam.getText().toString());
            body.put("image_url", imageTag);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(((App)getApplication()).isConnected()) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASEURL + ADDDEVICEURL,
                    body,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String token = null;

                            try {
                                token = response.getString("token");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String body = "";
                            if (error.networkResponse.data != null) {
                                try {
                                    body = new String(error.networkResponse.data, "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

            ) {
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Authorization", "Bearer " + Prefs.getString("userToken", null));
                    return headers;
                }
            };

            Request request = jsonObjectRequest;
            db.deviceDAO().insertDevice(new Device(inputApparaatNaam.getText().toString(), imageTag, null));
            List<Device> list = db.deviceDAO().getDevices();

            volleySingleton.addToRequestQueue(request);
            openHomeActivity();
        } else{
            new AlertDialog.Builder(this)
                    .setTitle("Geen verbinding")
                    .setMessage("Oh nee! Het lijkt erop dat je geen verbinding met het internet hebt.\nMaak verbinding en probeer het dan nog een keer.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    public void imageChosen(View view) {
        CircularImageView image = (CircularImageView) view;
        mCircularImageView.setImageDrawable(image.getDrawable());
        imageTag = (String) image.getTag();
    }
}