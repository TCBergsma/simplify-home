package com.example.simplify_home;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import com.example.simplify_home.broadcast.TimerBroadcast;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.services.TimerService;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Calendar;

public class App extends Application {
    public static final String CHANNEL_1_ID = "header_channel";
    public static final String CHANNEL_NOTIFICATION_ID = "notification_channel";
    public static final String CHANNEL_2_ID = "description_channel";
    private AppDatabase db;
    private AlarmManager alarmManager;

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        createNotificationChannels();
        db = AppDatabase.getInstance(App.this);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
    }

    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "Timer Notification In App",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This notification shows the timer that has finished.");

            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_NOTIFICATION_ID,
                    "Timer Notification",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This notification shows the timer that has finished.");

            NotificationChannel channel2 = new NotificationChannel(
                    CHANNEL_2_ID,
                    "Header",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel2.setDescription("This is the description. It shows the message you are receiving.");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(notificationChannel);
            manager.createNotificationChannel(channel2);
        }
    }

    public AppDatabase getDb() {
        return db;
    }

    public boolean isConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void createTimerNotification(Long calendar) {
        Intent intent = new Intent(this, TimerBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(App.this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar, pendingIntent);
    }
}
