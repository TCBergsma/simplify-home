package com.example.simplify_home;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.simplify_home.database.VolleySingleton;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity {

    VolleySingleton volleySingleton;
    Button buttonLogin;
    EditText textEmail;
    EditText textPassword;

    final private String BASEURL = "http://192.168.0.176:8000"; // M: http://192.168.178.9:8000 C: http://192.168.0.176:8000
    final private String LOGINURL = "/api/auth/login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String userToken = null;
        userToken = Prefs.getString("userToken", null);

        if (userToken !=  null) {
            setContentView(R.layout.activity_home);
            Intent toHomeScreenIntent = new Intent(this, HomeActivity.class);
            startActivity(toHomeScreenIntent);
        }

        setContentView(R.layout.activity_login);

        volleySingleton = VolleySingleton.getInstance(this.getApplicationContext());

        buttonLogin = findViewById(R.id.buttonRegister);
        textEmail = findViewById(R.id.textName);
        textPassword = findViewById(R.id.textPassword);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textEmail.getText().toString().matches("") && !textPassword.getText().toString().matches("")) {
                    loginNow();
                } else {
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Legen inputvelden")
                            .setMessage("Vul alle inputvelden in om in te kunnen loggen.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });
    }

    public void onClick(View v) {
        openRegisterActivity();
    }

    private void openHomeActivity() {
        Intent toHomeScreenIntent = new Intent(this, HomeActivity.class);
        startActivity(toHomeScreenIntent);
    }

    private void openRegisterActivity() {
        Intent toRegisterScreenIntent = new Intent(this, RegisterActivity.class);
        startActivity(toRegisterScreenIntent);
    }

    private void loginNow() {
        JSONObject body = new JSONObject();
        try {
            body.put("email", textEmail.getText().toString());
            body.put("password", textPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (((App) getApplication()).isConnected()) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASEURL + LOGINURL,
                    body,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String token = null;
                            String userToken = null;

                            try {
                                token = response.getString("token");
                                Prefs.putString("userToken", token);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            userToken = Prefs.getString("userToken", null);
                            openHomeActivity();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String body = "";
                            String errorMessage = "Controleer uw gegevens voor enkele typfouten.";

                            if (error.networkResponse.data != null) {
                                try {
                                    body = new String(error.networkResponse.data, "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }

                            String[] errorMessageArray = {"Unauthorized", "The email must be a valid email address.", "The confirm password must be at least 8 characters."};
                            String[] errorMessageArrayNL = {"Je ingevoerde gegevens kloppen niet, kijk of het wachtwoord klopt met je e-mailadres of maak een nieuw account aan.", "Voer een geldige e-mailadres in.", "Je wachtwoord moet langer zijn dan 8 karakters."};
                            for (int i = 0; i < errorMessageArray.length; i++) {
                                if (body.indexOf(errorMessageArray[i]) != -1) {
                                    if (errorMessage == "Controleer uw gegevens voor enkele typfouten.") {
                                        errorMessage = errorMessageArrayNL[i];
                                    } else {
                                        errorMessage = errorMessage + System.getProperty("line.separator") + errorMessageArrayNL[i];
                                    }
                                }
                            }
                            new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("Gegevens kloppen niet")
                                    .setMessage(errorMessage)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                    }
            );

            Request request = jsonObjectRequest;

            volleySingleton.addToRequestQueue(request);
        }else{
            new AlertDialog.Builder(this)
                    .setTitle("Geen verbinding")
                    .setMessage("Oh nee! Het lijkt erop dat je geen verbinding met het internet hebt.\nMaak verbinding en probeer het dan nog een keer.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }
}
