package com.example.simplify_home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.simplify_home.database.VolleySingleton;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ListAanpassenActivity extends AppCompatActivity {

    VolleySingleton volleySingleton;
    ArrayList<ProductListItems> listItems = new ArrayList<>();

    private int AantalProducten = 0;
    private int list_id = 0;
    ArrayList<ProductItem> producten = new ArrayList<>();
    Button buttonSaveEdit;
    Button buttonBackEdit;
    EditText textTitel;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_aanpassen);

        volleySingleton = VolleySingleton.getInstance(getApplicationContext());

        recyclerView = findViewById(R.id.recyclerViewProducten);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.hasFixedSize();

        buttonSaveEdit = findViewById(R.id.buttonSaveEdit);
        buttonBackEdit = findViewById(R.id.buttonBackEdit);
        textTitel = findViewById(R.id.textTitel);

        if(getIntent().hasExtra("title")) {
            textTitel.setText(getIntent().getExtras().getString("title"));
            getInformation(getIntent().getExtras().getString("title"));
        }

        buttonSaveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveList(list_id, getIntent().getExtras().getString("title"));
            }
        });

        buttonBackEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openListDetailActivity();
            }
        });
    }

    private void getInformation(String title){
        JSONObject body = new JSONObject();
        try {
            body.put("titel", title);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(this).getList(title, body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                int b = 0;

                for(Iterator<String> iter = ((JSONObject)response).keys(); iter.hasNext();) {
                    try {
                        JSONObject item = ((JSONObject)response).getJSONObject(iter.next());
                        list_id = Integer.parseInt(item.getString("list_id"));
                        listItems.add(new ProductListItems(b, Integer.parseInt(item.getString("count")), item.getString("product")));
                        b++;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                AantalProducten = b;
                recyclerViewAdapter = new ListItemsAanpassenAdapter(listItems, new ProductListItemAdapter.TextUpdatedListener() {
                    @Override
                    public void callback(int index, String text) {
                        listItems.get(index).setName(text);
                    }

                    @Override
                    public void callbackCount(int index, int count) {
                        listItems.get(index).setCount(count);
                    }
                });
                recyclerView.setAdapter(recyclerViewAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    public void onClick(View v) {
        if (v.toString().contains("textAddProduct")) {
            AantalProducten = AantalProducten + 1;
            listItems.add(new ProductListItems(AantalProducten, 0, ""));

            recyclerViewAdapter = new ListItemsAanpassenAdapter(listItems, new ProductListItemAdapter.TextUpdatedListener() {
                @Override
                public void callback(int index, String text) {
                    listItems.get(index).setName(text);
                }

                @Override
                public void callbackCount(int index, int count) {
                    listItems.get(index).setCount(count);
                }
            });
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.invalidate();
        } else if (v.toString().contains("textDeleteProduct")) {
            if (AantalProducten != 0) {
                AantalProducten = AantalProducten - 1;
                listItems.remove(AantalProducten);
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.invalidate();
            }
        }
    }

    private void openListDetailActivity() {
        Intent toListDetailScreenIntent = new Intent(this, ListDetailActivity.class);
        toListDetailScreenIntent.putExtra("title", textTitel.getText().toString());
        startActivity(toListDetailScreenIntent);
    }

    private void SaveList(int list_id, String title){
        JSONObject body = new JSONObject();
        try {
            body.put("titel", textTitel.getText().toString());
            body.put("TotalItems", AantalProducten);
            for (int c = 0; c < AantalProducten; c++) {
                body.put("count" + c, listItems.get(c).getCount());
                body.put("product" + c, listItems.get(c).getName());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(this).updateList(list_id, body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                openListDetailActivity();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }
}
