package com.example.simplify_home.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.example.simplify_home.services.TimerService;

public class TimerBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent newIntent = new Intent(context, TimerService.class);
        if (Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(newIntent);
        } else {
            // Pre-O behavior.
            context.startService(newIntent);
        }
    }

}
