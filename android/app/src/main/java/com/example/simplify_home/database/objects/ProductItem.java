package com.example.simplify_home.database.objects;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ProductItem")
public class ProductItem {
    @PrimaryKey
    @ColumnInfo(name = "id")
    public int id;
    @ColumnInfo(name = "status")
    public String status;
    @ColumnInfo(name = "count")
    public String count;
    @ColumnInfo(name = "product")
    public String product;

    public ProductItem(Integer id, String status, String count, String product){
        this.id = id;
        this.status = status;
        this.count = count;
        this.product = product;
    }
}
