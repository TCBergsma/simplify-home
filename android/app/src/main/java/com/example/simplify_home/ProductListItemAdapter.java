package com.example.simplify_home;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ProductListItemAdapter extends RecyclerView.Adapter<ProductListItemAdapter.ProductListItemViewHolder> {

    private ArrayList<ProductItem> producten;
    private TextUpdatedListener listener;

    public ProductListItemAdapter(ArrayList<ProductItem> producten, TextUpdatedListener listener) {
        this.producten = producten;
        this.listener = listener;
    }

    public static class ProductListItemViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout linearLayout;
        public EditText editTextAantal;
        public TextView textView;
        public EditText editTextProduct;

        public ProductListItemViewHolder(View v){
            super(v);
            linearLayout = v.findViewById(R.id.linearLayout);
            editTextAantal = v.findViewById(R.id.editTextAantal);
            textView = v.findViewById(R.id.textView);
            editTextProduct = v.findViewById(R.id.editTextProduct);
        }
    }

    @NonNull
    @Override
    public ProductListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_listitem_layout,parent,false);
        ProductListItemViewHolder productListItemViewHolder = new ProductListItemViewHolder(v);
        return productListItemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListItemViewHolder holder, final int position) {
        holder.editTextAantal.setText(Integer.toString(producten.get(position).getCount()));
        holder.textView.setText("x");
        holder.editTextProduct.setText(producten.get(position).getName());
        holder.editTextProduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listener.callback(position, s.toString());
            }
        });
        holder.editTextAantal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    listener.callbackCount(position, Integer.parseInt(s.toString()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return producten.size();
    }

    public interface TextUpdatedListener {
        public void callback(int index, String text);
        public void callbackCount(int index, int count);
    }
}
