package com.example.simplify_home;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.database.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class ListItemsAdapter extends RecyclerView.Adapter<ListItemsAdapter.ListItemsViewHolder> {
    private ArrayList<ProductListItems> listItems;
    private String title;
    private android.content.Context Context;
    public ListItemsAdapter(ArrayList<ProductListItems> listItems, String title) {
        this.listItems = listItems;
        this.title = title;
    }
    public static class ListItemsViewHolder extends RecyclerView.ViewHolder {
        public CheckBox checkBox;
        public ListItemsViewHolder(View v) {
            super(v);
            checkBox = v.findViewById(R.id.checkBox);
        }
    }



    @NonNull
    @Override
    public ListItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_layout,parent,false);
        ListItemsViewHolder listItemsViewHolder = new ListItemsViewHolder(v);
        return listItemsViewHolder;
    }

    private void getStatus(final CheckBox checkbox, final int position) {
        JSONObject body = new JSONObject();
        try {
            body.put("title", title);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(Context).getList(title, body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                for(Iterator<String> iter = ((JSONObject)response).keys(); iter.hasNext();) {
                    try {
                        JSONObject item = ((JSONObject)response).getJSONObject(iter.next());
                        if (listItems.get(position).getName().equals(item.getString("product"))) {
                            if (!item.getString("status").equals("open")) {
                                checkbox.setChecked(true);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(Context).addToRequestQueue(request);
    }

    private void updateStatus(String product, String status) {
        JSONObject body = new JSONObject();
        try {
            body.put("status", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(Context).updateStatusItem(product, body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(Context).addToRequestQueue(request);
    }


    @Override
    public void onBindViewHolder(@NonNull ListItemsViewHolder holder, final int position) {
        holder.checkBox.setText(listItems.get(position).getCount() + "x " + listItems.get(position).getName());
        getStatus(holder.checkBox, position);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                   if(isChecked){
                        updateStatus(listItems.get(position).getName(), "close");
                   } else {
                        updateStatus(listItems.get(position).getName(), "open");
                   }
               }
           }
        );
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

}
