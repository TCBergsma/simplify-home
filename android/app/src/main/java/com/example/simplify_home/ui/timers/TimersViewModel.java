package com.example.simplify_home.ui.timers;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TimersViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TimersViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is timer fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}