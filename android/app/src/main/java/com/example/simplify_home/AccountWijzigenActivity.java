package com.example.simplify_home;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.database.VolleySingleton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AccountWijzigenActivity extends AppCompatActivity {

    private TextInputEditText textInputAccountNaam;
    private TextInputEditText textInputAccountEmail;

    private Button buttonSaveAccount;
    private Button buttonSavePassword;
    private Button buttonBackAccountEdit;

    private TextView textViewPasswordReset;

    private TextInputLayout textInputHuidigWachtwoordLayout;
    private TextInputLayout textInputNieuwWachtwoordLayout;
    private TextInputLayout textInputNieuwWachtwoordHerhalenLayout;

    private TextInputEditText textInputHuidigWachtwoord;
    private TextInputEditText textInputNieuwWachtwoord;
    private TextInputEditText textInputNieuwWachtwoordHerhalen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_wijzigen);

        textInputAccountNaam = findViewById(R.id.textInputAccountNaam);
        textInputAccountEmail = findViewById(R.id.textInputAccountEmail);

        buttonSaveAccount = findViewById(R.id.buttonSaveAccount);
        buttonSavePassword = findViewById(R.id.buttonSavePassword);
        buttonBackAccountEdit = findViewById(R.id.buttonBackAccountEdit);

        textViewPasswordReset = findViewById(R.id.textViewPasswordReset);

        textInputHuidigWachtwoordLayout = findViewById(R.id.textInputHuidigWachtwoordLayout);
        textInputNieuwWachtwoordLayout = findViewById(R.id.textInputNieuwWachtwoordLayout);
        textInputNieuwWachtwoordHerhalenLayout = findViewById(R.id.textInputNieuwWachtwoordHerhalenLayout);

        textInputHuidigWachtwoord = findViewById(R.id.textInputHuidigWachtwoord);
        textInputNieuwWachtwoord = findViewById(R.id.textInputNieuwWachtwoord);
        textInputNieuwWachtwoordHerhalen = findViewById(R.id.textInputNieuwWachtwoordHerhalen);


        getAccountDetails();

        buttonSaveAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserGegevens();
            }
        });

        buttonBackAccountEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textViewPasswordReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textInputHuidigWachtwoordLayout.setVisibility(v.VISIBLE);
                textInputNieuwWachtwoordLayout.setVisibility(v.VISIBLE);
                textInputNieuwWachtwoordHerhalenLayout.setVisibility(v.VISIBLE);
                buttonSavePassword.setVisibility(v.VISIBLE);
            }
        });

        buttonSavePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserWachtwoord();
            }
        });
    }

    private void getAccountDetails(){
        Request request = VolleySingleton.getInstance(this).getUser(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                ArrayList<String> naam = new ArrayList<>();
                ArrayList<String> email = new ArrayList<>();
                try {
                    JSONObject item = (JSONObject) response;
                    naam.add(item.getString("name"));
                    email.add(item.getString("email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                textInputAccountNaam.setText(naam.get(0));
                textInputAccountEmail.setText(email.get(0));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void updateUserGegevens(){
        JSONObject body = new JSONObject();
        try {
            body.put("name", textInputAccountNaam.getText().toString());
            body.put("email", textInputAccountEmail.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(this).updateUser(body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                new AlertDialog.Builder(AccountWijzigenActivity.this)
                        .setTitle("Succes!")
                        .setMessage("Je gegevens zijn succesvol gewijzigd.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new AlertDialog.Builder(AccountWijzigenActivity.this)
                        .setTitle("Sorry, niet gelukt!")
                        .setMessage("Het is niet gelukt om je gegevens te wijzigen. \nProbeer het later nog een keer")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);

    }

    private void updateUserWachtwoord(){
        JSONObject body = new JSONObject();
        try {
            body.put("password_current", textInputHuidigWachtwoord.getText().toString());
            body.put("password", textInputNieuwWachtwoord.getText().toString());
            body.put("password_confirmation", textInputNieuwWachtwoordHerhalen.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(this).updateUserPassword(body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                new AlertDialog.Builder(AccountWijzigenActivity.this)
                        .setTitle("Succes!")
                        .setMessage("Je wachtwoord is succesvol gewijzigd.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new AlertDialog.Builder(AccountWijzigenActivity.this)
                        .setTitle("Sorry, niet gelukt!")
                        .setMessage("Het is niet gelukt om je wachtwoord te wijzigen. \nControlleer of je huidige wachtwoord klopt en/of je het nieuwe wachtwoord gelijk is aan de herhaling")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);

    }
}