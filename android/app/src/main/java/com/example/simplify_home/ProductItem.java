package com.example.simplify_home;

public class ProductItem {
    private int index;
    private int count;
    private String name;

    public ProductItem (int index, String name) {
        this.index = index;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
