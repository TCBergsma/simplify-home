package com.example.simplify_home.services;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.simplify_home.App;
import com.example.simplify_home.R;

public class TimerService extends Service {
        @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        WindDownTask();
        if (intent == null) {
            return START_STICKY_COMPATIBILITY;
        }
        return START_STICKY;
    }
    private void WindDownTask() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(TimerService.this, "notificationChannel")
                .setSmallIcon(R.drawable.ic_house_lightbulb_black_24dp)
                .setContentTitle("Timer is afgelopen")
                .setContentText("Je gezette timer is afgelopen!")
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setColorized(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true);
        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(TimerService.this);
        managerCompat.notify(1, builder.build());

    }
    IBinder binder = null;
    @Override
    public IBinder onBind(Intent intent) {
        stopForeground(true);
        return binder;
    }
    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        stopForeground(true);
    }
    @Override
    public boolean onUnbind(Intent intent) {
        WindDownTask();
        return super.onUnbind(intent);
    }
}
