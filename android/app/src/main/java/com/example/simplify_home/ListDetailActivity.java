package com.example.simplify_home;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.database.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class ListDetailActivity extends AppCompatActivity {
    VolleySingleton volleySingleton;
    ArrayList<ProductListItems> listItems = new ArrayList<>();

    private int list_id = 0;

    private String title;
    private TextView textTitelList;
    private Button buttonEdit;
    private Button buttonBackDetail;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_detail);

        recyclerView = findViewById(R.id.recyclerViewCheckBoxs);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.hasFixedSize();

        recyclerViewAdapter = new ListItemsAdapter(listItems, title);
        recyclerView.setAdapter(recyclerViewAdapter);

        textTitelList = findViewById(R.id.textTitelList);
        buttonEdit = findViewById(R.id.buttonEdit);
        buttonBackDetail = findViewById(R.id.buttonBackDetail);

        if(getIntent().hasExtra("title")) {
            textTitelList.setText(getIntent().getExtras().getString("title"));
            getInformation(getIntent().getExtras().getString("title"));
        }

        buttonBackDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent toHome = new Intent(v.getContext(), HomeActivity.class);
            v.getContext().startActivity(toHome);
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toListAanpassen = new Intent(v.getContext(), ListAanpassenActivity.class);
                toListAanpassen.putExtra("title", getIntent().getExtras().getString("title"));
                v.getContext().startActivity(toListAanpassen);
            }
        });
    }

    public void onClick(View v) {
        if (v.toString().contains("textDeleteList")) {
            new AlertDialog.Builder(this)
                    .setTitle("Zeker weten?")
                    .setMessage("Weet je zeker dat je deze lijst wilt verwijderen?")
                    .setPositiveButton("Verwijderen", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteList();
                            openHomeActivity();
                        }
                    })
                    .setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    private void deleteList(){
        JSONObject body = new JSONObject();
        try {
            body.put("list_id", list_id);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(this).deleteList(list_id, body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void openHomeActivity(){
        Intent toHomeActivity = new Intent(this, HomeActivity.class);
        this.startActivity(toHomeActivity);
    }

    private void getInformation(final String title){
        JSONObject body = new JSONObject();
        try {
            body.put("titel", title);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(this).getList(title, body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                int a = 0;

                for(Iterator<String> iter = ((JSONObject)response).keys(); iter.hasNext();) {
                    try {
                        JSONObject item = ((JSONObject)response).getJSONObject(iter.next());
                        list_id = Integer.parseInt(item.getString("list_id"));
                        listItems.add(new ProductListItems(a, Integer.parseInt(item.getString("count")), item.getString("product")));
                        a++;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                recyclerViewAdapter = new ListItemsAdapter(listItems, title);
                recyclerView.setAdapter(recyclerViewAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }
}
