package com.example.simplify_home.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.simplify_home.database.dao.DeviceDAO;
import com.example.simplify_home.database.dao.ProductItemDAO;
import com.example.simplify_home.database.dao.ProductItemListDAO;
import com.example.simplify_home.database.objects.Device;
import com.example.simplify_home.database.objects.ProductItem;
import com.example.simplify_home.database.objects.ProductItemList;

@Database(entities = {ProductItem.class, ProductItemList.class, Device.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ProductItemDAO productItemDAO();
    public abstract ProductItemListDAO productItemListDAO();
    public abstract DeviceDAO deviceDAO();
    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }
    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "simplify_home").allowMainThreadQueries().fallbackToDestructiveMigration().build();
    }
}