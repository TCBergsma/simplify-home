package com.example.simplify_home.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.simplify_home.ProductItem;
import com.example.simplify_home.database.objects.Device;

import java.util.List;

@Dao
public interface DeviceDAO {
    @Query("UPDATE Device SET last_timer = :last_timer WHERE id = :id")
    void updateTimer(long id, long last_timer);

    @Query("UPDATE Device SET image_url = :image_url & name = :name WHERE id = :id")
    void updateDevice(long id, String image_url, String name);

    //Done
    @Insert
    void insertDevice(Device device);

    //Done
    @Query("SELECT * FROM Device")
    List<Device> getDevices();

    //Done
    @Query("SELECT * FROM Device WHERE id = :id")
    List<Device> getDevice(long id);

    //Done
    @Query("DELETE FROM Device WHERE id = :id")
    void removeDevice(long id);
}
