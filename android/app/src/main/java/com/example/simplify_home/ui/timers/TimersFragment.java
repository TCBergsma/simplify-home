package com.example.simplify_home.ui.timers;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Dao;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.App;
import com.example.simplify_home.ApparaatToevoegen;
import com.example.simplify_home.ProductItem;
import com.example.simplify_home.R;
import com.example.simplify_home.TimersAdapter;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.database.VolleySingleton;
import com.example.simplify_home.database.objects.Device;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TimersFragment extends Fragment {

    private TimersViewModel timersViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Button apparaatToevoegenButton;
    private AppDatabase db;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        timersViewModel = ViewModelProviders.of(this).get(TimersViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_timers, container, false);
        timersViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });

        db = ((App) getActivity().getApplication()).getDb();


        recyclerView = root.findViewById(R.id.gridViewApparaten);
        layoutManager = new GridLayoutManager(this.getActivity(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        apparaatToevoegenButton = root.findViewById(R.id.apparaatToevoegen);

        apparaatToevoegenButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent toApparaatToevoegen = new Intent(root.getContext(), ApparaatToevoegen.class);
                startActivity(toApparaatToevoegen);
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if(((App) getActivity().getApplication()).isConnected()) {
            Request request = VolleySingleton.getInstance(this.getContext()).getDevices(new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    ArrayList<String> namen = new ArrayList<>();
                    ArrayList<String> image = new ArrayList<>();
                    ArrayList<String> tijden = new ArrayList<>();
                    ArrayList<Integer> id = new ArrayList<>();

                    for (Iterator<String> iter = ((JSONObject) response).keys(); iter.hasNext(); ) {
                        try {
                            JSONObject item = ((JSONObject) response).getJSONObject(iter.next());
                            namen.add(item.getString("name"));
                            image.add(item.getString("image_url"));
                            id.add(item.getInt("id"));
                            tijden.add("");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    recyclerViewAdapter = new TimersAdapter(namen, image, tijden, id, getContext(), getActivity());
                    recyclerView.setAdapter(recyclerViewAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(this.getContext()).addToRequestQueue(request);
        }else{
            ArrayList<String> namen = new ArrayList<>();
            ArrayList<String> image = new ArrayList<>();
            ArrayList<String> tijden = new ArrayList<>();
            ArrayList<Integer> id = new ArrayList<>();

            List<Device> list = db.deviceDAO().getDevices();
            for(Device device : list){
                namen.add(device.name);
                image.add(device.image_url);
                id.add(device.id);
                tijden.add("");
            }
            recyclerViewAdapter = new TimersAdapter(namen, image, tijden, id, getContext(), getActivity());
            recyclerView.setAdapter(recyclerViewAdapter);
        }
    }
}