package com.example.simplify_home.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.App;
import com.example.simplify_home.R;
import com.example.simplify_home.ShoppinglistAdapter;
import com.example.simplify_home.TimersAdapter;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.database.VolleySingleton;
import com.example.simplify_home.database.objects.Device;
import com.example.simplify_home.database.objects.ProductItemList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView recyclerView;
    private RecyclerView recyclerViewShoppinglist;

    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.Adapter recyclerViewAdapterShoppinglist;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.LayoutManager layoutManagerShoppinglist;

    private AppDatabase db;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        db = ((App) getActivity().getApplication()).getDb();

        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        return root;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView = view.findViewById(R.id.gridView);
        recyclerViewShoppinglist = view.findViewById(R.id.shoppinglistView);

        layoutManager = new GridLayoutManager(getContext(), 2);
        layoutManagerShoppinglist = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerViewShoppinglist.setLayoutManager(layoutManagerShoppinglist);

        recyclerView.setNestedScrollingEnabled(false);

        //// GET DEVICES
        if (((App) getActivity().getApplication()).isConnected()) {
            Request requestDevices = VolleySingleton.getInstance(this.getContext()).getDevices(new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    ArrayList<String> namen = new ArrayList<>();
                    ArrayList<String> image = new ArrayList<>();
                    ArrayList<Integer> id = new ArrayList<>();
                    ArrayList<String> tijden = new ArrayList<>();

                    for (Iterator<String> iter = ((JSONObject) response).keys(); iter.hasNext(); ) {
                        try {
                            JSONObject item = ((JSONObject) response).getJSONObject(iter.next());
                            namen.add(item.getString("name"));
                            image.add(item.getString("image_url"));
                            id.add(item.getInt("id"));
                            tijden.add("");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    recyclerViewAdapter = new TimersAdapter(namen, image, tijden, id, getContext(), getActivity());
                    recyclerView.setAdapter(recyclerViewAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(this.getContext()).addToRequestQueue(requestDevices);
        } else {
            ArrayList<String> namen = new ArrayList<>();
            ArrayList<String> image = new ArrayList<>();
            ArrayList<String> tijden = new ArrayList<>();
            ArrayList<Integer> id = new ArrayList<>();

            List<Device> list = db.deviceDAO().getDevices();
            for (Device device : list) {
                namen.add(device.name);
                image.add(device.image_url);
                id.add(device.id);
                tijden.add("");
            }
            recyclerViewAdapter = new TimersAdapter(namen, image, tijden, id, getContext(), getActivity());
            recyclerView.setAdapter(recyclerViewAdapter);
        }

        //// GET LISTS
        if (((App) getActivity().getApplication()).isConnected()) {
            Request requestLists = VolleySingleton.getInstance(this.getContext()).getLists(new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    ArrayList<String> titles = new ArrayList<>();

                    for (Iterator<String> iter = ((JSONObject) response).keys(); iter.hasNext(); ) {
                        try {
                            JSONObject item = ((JSONObject) response).getJSONObject(iter.next());
                            titles.add(item.getString("titel"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    recyclerViewAdapterShoppinglist = new ShoppinglistAdapter(titles, getActivity());
                    recyclerViewShoppinglist.setAdapter(recyclerViewAdapterShoppinglist);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            VolleySingleton.getInstance(this.getContext()).addToRequestQueue(requestLists);
        }else{
            ArrayList<String> titles = new ArrayList<>();

            List<ProductItemList> list = db.productItemListDAO().getLists();
            for (ProductItemList productItemList : list) {
                titles.add(productItemList.titel);
            }

            recyclerViewAdapterShoppinglist = new ShoppinglistAdapter(titles, getActivity());
            recyclerViewShoppinglist.setAdapter(recyclerViewAdapterShoppinglist);
        }
    }
}