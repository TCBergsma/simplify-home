package com.example.simplify_home;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.simplify_home.database.VolleySingleton;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class RegisterActivity extends AppCompatActivity {

    VolleySingleton volleySingleton;
    Button buttonRegister;
    EditText textEmail;
    EditText textName;
    EditText textPassword;
    EditText textVerifiedPassword;

    final private String BASEURL = "http://192.168.0.176:8000"; // M: http://192.168.178.9:8000 C: http://192.168.0.176:8000
    final private String SIGNUPURL = "/api/auth/signup";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        volleySingleton = VolleySingleton.getInstance(this.getApplicationContext());

        buttonRegister = findViewById(R.id.buttonRegister);
        textEmail = findViewById(R.id.textEmail);
        textName = findViewById(R.id.textName);
        textPassword = findViewById(R.id.textPassword);
        textVerifiedPassword = findViewById(R.id.textVerifiedPassword);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textEmail.getText().toString().matches("") && !textName.getText().toString().matches("") && !textEmail.getText().toString().matches("") && !textVerifiedPassword.getText().toString().matches("")) {
                    signupNow();
                } else {
                    new AlertDialog.Builder(RegisterActivity.this)
                        .setTitle("Legen inputvelden")
                        .setMessage("Vul alle inputvelden in om een account aan te kunnen maken.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                }
            }
        });
    }

    public void onClick(View v) {
        openLoginActivity();
    }

    private void openHomeActivity() {
        Intent toHomeScreenIntent = new Intent(this,HomeActivity.class);
        startActivity(toHomeScreenIntent);
    }

    private void openLoginActivity() {
        Intent toLoginScreenIntent = new Intent(this,LoginActivity.class);
        startActivity(toLoginScreenIntent);
    }

    private void signupNow() {
        JSONObject body = new JSONObject();
        try {
            body.put("name", textName.getText().toString());
            body.put("email", textEmail.getText().toString());
            body.put("password", textPassword.getText().toString());
            body.put("confirm_password", textVerifiedPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (((App) getApplication()).isConnected()) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASEURL + SIGNUPURL,
                    body,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            String token = null;

                            try {
                                token = response.getString("token");
                                Prefs.putString("userToken", token);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            openHomeActivity();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String body = "";
                            String errorMessage = "Controleer uw gegevens voor enkele typfouten.";

                            if (error.networkResponse.data != null) {
                                try {
                                    body = new String(error.networkResponse.data, "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                            String[] errorMessageArray = {"The email has already been taken.", "The email must be a valid email address.", "The confirm password and password must match.", "The confirm password must be at least 8 characters."};
                            String[] errorMessageArrayNL = {"Je ingevoerde e-mailadres is al in gebruik.", "Voer een geldige e-mailadres in.", "Je herhaalde wachtwoord is niet gelijk met je wachtwoord.", "Je wachtwoord moet langer zijn dan 8 karakters."};
                            for (int i = 0; i < errorMessageArray.length; i++) {
                                if (body.indexOf(errorMessageArray[i]) != -1) {
                                    if (errorMessage == "Controleer uw gegevens voor enkele typfouten.") {
                                        errorMessage = errorMessageArrayNL[i];
                                    } else {
                                        errorMessage = errorMessage + System.getProperty("line.separator") + errorMessageArrayNL[i];
                                    }
                                }
                            }
                            new AlertDialog.Builder(RegisterActivity.this)
                                    .setTitle("Gegevens kloppen niet")
                                    .setMessage(errorMessage)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                    }
            );

            Request request = jsonObjectRequest;

            volleySingleton.addToRequestQueue(request);
        }else{
            new AlertDialog.Builder(this)
                    .setTitle("Geen verbinding")
                    .setMessage("Oh nee! Het lijkt erop dat je geen verbinding met het internet hebt.\nMaak verbinding en probeer het dan nog een keer.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }
}