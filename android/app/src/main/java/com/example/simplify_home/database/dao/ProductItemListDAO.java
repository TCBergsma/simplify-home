package com.example.simplify_home.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.simplify_home.database.objects.Device;
import com.example.simplify_home.database.objects.ProductItemList;

import java.util.List;

@Dao
public interface ProductItemListDAO {
//    @Query("UPDATE Device SET image_url = :image_url & name = :name WHERE id = :id")
//    void updateDevice(long id, String image_url, String name);

    //Done
    @Insert
    void insertList(ProductItemList productItemList);

    //Done
    @Query("SELECT * FROM ProductItemList")
    List<ProductItemList> getLists();

//    //Done
//    @Query("SELECT * FROM ProductItemList WHERE id = :id")
//    List<ProductItemList> getList(long id);
//
//    //Done
//    @Query("DELETE FROM ProductItemList WHERE id = :id")
//    void removeList(long id);
}
