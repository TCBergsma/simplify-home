package com.example.simplify_home.ui.shoppinglist;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.App;
import com.example.simplify_home.R;
import com.example.simplify_home.ShoppinglistAdapter;
import com.example.simplify_home.ShoppinglistToevoegenActivity;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.database.VolleySingleton;
import com.example.simplify_home.database.objects.ProductItemList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShoppinglistFragment extends Fragment {

    private ShoppinglistViewModel shoppinglistViewModel;
    private RecyclerView recyclerViewShoppinglist;
    private RecyclerView.Adapter recyclerViewAdapterShoppinglist;
    private RecyclerView.LayoutManager layoutManagerShoppinglist;
    private AppDatabase db;

    Button buttonAddShoppinglist;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        shoppinglistViewModel = ViewModelProviders.of(this).get(ShoppinglistViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_shoppinglist, container, false);
        db = ((App) getActivity().getApplication()).getDb();

        shoppinglistViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });

        recyclerViewShoppinglist = root.findViewById(R.id.shoppinglistView);
        layoutManagerShoppinglist = new LinearLayoutManager(getContext());
        recyclerViewShoppinglist.setLayoutManager(layoutManagerShoppinglist);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        buttonAddShoppinglist = view.findViewById(R.id.buttonAddShoppinglist);
        buttonAddShoppinglist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddShoppinglistFragment();
            }
        });
        //// GET LISTS
        if (((App) getActivity().getApplication()).isConnected()) {
            Request requestLists = VolleySingleton.getInstance(this.getContext()).getLists(new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    ArrayList<String> titles = new ArrayList<>();

                    for (Iterator<String> iter = ((JSONObject) response).keys(); iter.hasNext(); ) {
                        try {
                            JSONObject item = ((JSONObject) response).getJSONObject(iter.next());
                            titles.add(item.getString("titel"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    recyclerViewAdapterShoppinglist = new ShoppinglistAdapter(titles, getActivity());
                    recyclerViewShoppinglist.setAdapter(recyclerViewAdapterShoppinglist);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            VolleySingleton.getInstance(this.getContext()).addToRequestQueue(requestLists);
        }else{
            ArrayList<String> titles = new ArrayList<>();

            List<ProductItemList> list = db.productItemListDAO().getLists();
            for (ProductItemList productItemList : list) {
                titles.add(productItemList.titel);
            }

            recyclerViewAdapterShoppinglist = new ShoppinglistAdapter(titles, getActivity());
            recyclerViewShoppinglist.setAdapter(recyclerViewAdapterShoppinglist);
        }
    }

    private void openAddShoppinglistFragment() {
        Intent toAddShoppinglistScreenIntent = new Intent(getContext(), ShoppinglistToevoegenActivity.class);
        startActivity(toAddShoppinglistScreenIntent);
    }
}