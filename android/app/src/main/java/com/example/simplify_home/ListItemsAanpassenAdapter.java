package com.example.simplify_home;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

class ListItemsAanpassenAdapter extends RecyclerView.Adapter<ListItemsAanpassenAdapter.ListItemsAanpassenViewHolder> {

    ArrayList<ProductListItems> listItems = new ArrayList<>();
    private ProductListItemAdapter.TextUpdatedListener listener;

    public ListItemsAanpassenAdapter(ArrayList<ProductListItems> listItems, ProductListItemAdapter.TextUpdatedListener listener) {
        this.listItems = listItems;
        this.listener = listener;
    }

    public static class ListItemsAanpassenViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout linearLayout;
        public EditText editTextAantal;
        public TextView textView;
        public EditText editTextProduct;

        public ListItemsAanpassenViewHolder(View v){
            super(v);
            linearLayout = v.findViewById(R.id.linearLayout);
            editTextAantal = v.findViewById(R.id.editTextAantal);
            textView = v.findViewById(R.id.textView);
            editTextProduct = v.findViewById(R.id.editTextProduct);
        }
    }

    @NonNull
    @Override
    public ListItemsAanpassenAdapter.ListItemsAanpassenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_listitem_layout,parent,false);
        ListItemsAanpassenAdapter.ListItemsAanpassenViewHolder listItemsAanpassenViewHolder = new ListItemsAanpassenAdapter.ListItemsAanpassenViewHolder(v);
        return listItemsAanpassenViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemsAanpassenAdapter.ListItemsAanpassenViewHolder holder, final int position) {
        holder.editTextAantal.setText(Integer.toString(listItems.get(position).getCount()));
        holder.textView.setText("x");
        holder.editTextProduct.setText(listItems.get(position).getName());
        holder.editTextProduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listener.callback(position, s.toString());
            }
        });
        holder.editTextAantal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    listener.callbackCount(position, Integer.parseInt(s.toString()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public interface TextUpdatedListener {
        public void callback(int index, String text);
        public void callbackCount(int index, int count);
    }

}
