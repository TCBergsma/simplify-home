package com.example.simplify_home.database.objects;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ProductItemList")
public class ProductItemList {
    @PrimaryKey
    @ColumnInfo(name = "id")
    public int id;
    @ColumnInfo(name = "titel")
    public String titel;

    public ProductItemList(String titel){
        this.titel = titel;
    }
}
