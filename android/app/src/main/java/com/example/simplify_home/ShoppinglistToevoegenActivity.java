package com.example.simplify_home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.database.VolleySingleton;
import com.example.simplify_home.database.objects.Device;
import com.example.simplify_home.database.objects.ProductItemList;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppinglistToevoegenActivity extends AppCompatActivity {

    final private String BASEURL = "http://192.168.0.176:8000"; // M: http://192.168.178.9:8000 C: http://192.168.0.176:8000
    final private String ADDLISTURL = "/api/auth/add-list";

    VolleySingleton volleySingleton;
    ArrayList<ProductListItems> listItems = new ArrayList<>();

    private int AantalProducten = 0;
    ArrayList<ProductItem> producten = new ArrayList<>();
    Button buttonOpslaan;
    Button buttonAnnuleren;
    EditText textTitel;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addlist);

        volleySingleton = VolleySingleton.getInstance(getApplicationContext());

        recyclerView = findViewById(R.id.recyclerViewProducten);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.hasFixedSize();

        db = ((App) getApplicationContext()).getDb();

        for (int i = 0; i < 4; i++) {
            producten.add(new ProductItem(AantalProducten, ""));
            AantalProducten = AantalProducten + 1;
        }

        recyclerViewAdapter = new ProductListItemAdapter(producten, new ProductListItemAdapter.TextUpdatedListener() {
            @Override
            public void callback(int index, String text) {
                producten.get(index).setName(text);
            }

            @Override
            public void callbackCount(int index, int count) {
                producten.get(index).setCount(count);
            }
        });
        recyclerView.setAdapter(recyclerViewAdapter);

        buttonOpslaan = findViewById(R.id.buttonOpslaan);
        buttonAnnuleren = findViewById(R.id.buttonAnnuleren);
        textTitel = findViewById(R.id.textTitel);

        buttonOpslaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int a = 0; a < AantalProducten; a++) {
                    listItems.add(new ProductListItems(a, producten.get(a).getCount(), producten.get(a).getName()));
                }
                SaveList();
            }
        });

        buttonAnnuleren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHomeActivity();
            }
        });
    }

    public void onClick(View v) {
        if (v.toString().contains("textAddProduct")) {
            AantalProducten = AantalProducten + 1;
            producten.add(new ProductItem(AantalProducten, ""));

            recyclerViewAdapter = new ProductListItemAdapter(producten, new ProductListItemAdapter.TextUpdatedListener() {
                @Override
                public void callback(int index, String text) {
                    producten.get(index).setName(text);
                }

                @Override
                public void callbackCount(int index, int count) {
                    producten.get(index).setCount(count);
                }
            });
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.invalidate();
        } else if (v.toString().contains("textDeleteProduct")) {
            if (AantalProducten != 0) {
                AantalProducten = AantalProducten - 1;
                producten.remove(AantalProducten);
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.invalidate();
            }
        }
    }

    private void openHomeActivity() {
        Intent toHomeScreenIntent = new Intent(this, HomeActivity.class);
        startActivity(toHomeScreenIntent);
    }

    private void SaveList(){
        JSONObject body = new JSONObject();
        try {
            body.put("titel", textTitel.getText().toString());
            body.put("TotalItems", AantalProducten);
            for (int b = 0; b < AantalProducten; b++) {
                body.put("count" + b, listItems.get(b).getCount());
                body.put("product" + b, listItems.get(b).getName());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = new JsonObjectRequest(
                Request.Method.POST,
                BASEURL + ADDLISTURL,
                body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String token = null;

                        try {
                            token = response.getString("token");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        openHomeActivity();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = "";
                        if (error.networkResponse.data != null) {
                            try {
                                body = new String(error.networkResponse.data, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        ){
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Authorization", "Bearer " + Prefs.getString("userToken", null));
                return headers;
            }
        };

        db.productItemListDAO().insertList(new ProductItemList(textTitel.getText().toString()));
        List<ProductItemList> list = db.productItemListDAO().getLists();
        volleySingleton.addToRequestQueue(request);
    }
}
