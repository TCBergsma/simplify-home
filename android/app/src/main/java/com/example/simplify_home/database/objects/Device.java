package com.example.simplify_home.database.objects;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Device")
public class Device {
    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    public int id;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "image_url")
    public String image_url;
    @ColumnInfo(name = "last_timer")
    @Nullable
    public Integer last_timer;
    @ColumnInfo(name = "backend_id")
    @Nullable
    public int backend_id;

    public Device(String name, String image_url, Integer last_timer){
        this.name = name;
        this.image_url = image_url;
        this.last_timer = last_timer;
    }
}
