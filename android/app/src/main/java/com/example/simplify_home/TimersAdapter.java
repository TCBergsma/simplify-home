package com.example.simplify_home;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class TimersAdapter extends RecyclerView.Adapter<TimersAdapter.TimersViewHolder> {

    private ArrayList<String> namen;
    private ArrayList<String> image_url;
    private ArrayList<String> tijden;
    private ArrayList<Integer> id;
    private Context mContext;
    private Activity mActivity;


    public TimersAdapter(ArrayList<String> namen, ArrayList<String> image_url, ArrayList<String> tijden, ArrayList<Integer> id, Context context, Activity activity){
        this.namen = namen;
        this.image_url = image_url;
        this.tijden = tijden;
        this.id = id;
        mContext = context;
        mActivity = activity;
    }

    public static class TimersViewHolder extends RecyclerView.ViewHolder{
        public TextView apparaatNaam;
        public TextView apparaatTimer;
        public CircularImageView apparaatImage;
        public View mImage;
        public ConstraintLayout parentLayout;


        public TimersViewHolder(View v){
            super(v);
            apparaatNaam = v.findViewById(R.id.apparaatNaam);
            apparaatTimer = v.findViewById(R.id.apparaatTimer);
            apparaatImage = v.findViewById(R.id.apparaatImage);
            parentLayout = v.findViewById(R.id.parent_layout);

        }
    }

    @NonNull
    @Override
    public TimersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.apparaat_timer_layout,parent,false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toApparaatDetail = new Intent(v.getContext(), ApparaatDetailActivity.class);
                toApparaatDetail.putExtra("id", id);
                v.getContext().startActivity(toApparaatDetail);
            }
        });
        TimersViewHolder timersViewHolder = new TimersViewHolder(v);
        return timersViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final TimersViewHolder holder, final int position) {
        holder.apparaatNaam.setText(namen.get(position));
        holder.apparaatTimer.setText(tijden.get(position));

        final Context context = holder.apparaatImage.getContext();
        int url = context.getResources().getIdentifier(image_url.get(position), "drawable", context.getPackageName());
        holder.apparaatImage.setImageResource(url);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ApparaatDetailActivity.class);
                intent.putExtra("id", id.get(position));
                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mActivity, holder.apparaatImage, "imageSlide");
                mContext.startActivity(intent, activityOptionsCompat.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return namen.size();
    }

}
