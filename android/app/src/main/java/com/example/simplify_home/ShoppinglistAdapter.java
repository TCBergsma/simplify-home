package com.example.simplify_home;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ShoppinglistAdapter extends RecyclerView.Adapter<ShoppinglistAdapter.ShoppinglistViewHolder> {
    private ArrayList<String> titles;
    private Activity mactivity;

    public ShoppinglistAdapter(ArrayList<String> titles, Activity activity){
        this.titles = titles;
        this.mactivity = activity;
    }

    public static class ShoppinglistViewHolder extends RecyclerView.ViewHolder{
        public Button shoppinglistButton;

        public ShoppinglistViewHolder(View v){
            super(v);
            shoppinglistButton = v.findViewById(R.id.shoppinglistButton);
        }
    }

    @NonNull
    @Override
    public ShoppinglistAdapter.ShoppinglistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.booschappenlijst_card_layout,parent,false);
        ShoppinglistAdapter.ShoppinglistViewHolder shoppinglistViewHolder = new ShoppinglistViewHolder(v);
        return shoppinglistViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppinglistAdapter.ShoppinglistViewHolder holder, final int position) {
        holder.shoppinglistButton.setText(titles.get(position));

        holder.shoppinglistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((App) mactivity.getApplication()).isConnected()) {
                    Intent intent = new Intent(v.getContext(), ListDetailActivity.class);
                    intent.putExtra("title", titles.get(position));
                    v.getContext().startActivity(intent);
                }else{
                    new AlertDialog.Builder(mactivity)
                            .setTitle("Geen verbinding")
                            .setMessage("Oh nee! Het lijkt erop dat je geen verbinding met het internet hebt.\nMaak verbinding en probeer het dan nog een keer.\nDeze functie is op dit moment nog niet werkend.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return titles.size();
    }
}
