package com.example.simplify_home;

public class ProductListItems {
    private int index;
    private int count;
    private String name;

    public ProductListItems (int index, int count, String name) {
        this.index = index;
        this.count = count;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
