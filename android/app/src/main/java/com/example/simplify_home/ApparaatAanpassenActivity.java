package com.example.simplify_home;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.simplify_home.database.AppDatabase;
import com.example.simplify_home.database.VolleySingleton;
import com.example.simplify_home.database.objects.Device;
import com.google.android.material.textfield.TextInputEditText;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ApparaatAanpassenActivity extends AppCompatActivity {

    private Button buttonBackEdit;
    private Button buttonSaveEdit;
    private TextView deleteDevice;
    private TextInputEditText inputEditApparaatNaam;
    private CircularImageView apparaatImageToevoegen;
    private String imageTag = "image";
    private Integer contentId;
    VolleySingleton volleySingleton;
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apparaat_aanpassen);
        getIncomingIntent();

        buttonBackEdit = findViewById(R.id.buttonBackEdit);
        inputEditApparaatNaam = findViewById(R.id.inputEditApparaatNaam);
        apparaatImageToevoegen = findViewById(R.id.apparaatImageToevoegen);
        buttonSaveEdit = findViewById(R.id.buttonSaveEdit);
        deleteDevice = findViewById(R.id.deleteDevice);
        db = ((App) getApplicationContext()).getDb();

        buttonSaveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateDevice(contentId);
                openHomeActivity();
            }
        });

        buttonBackEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toApparaatDetail = new Intent(v.getContext(), ApparaatDetailActivity.class);
                toApparaatDetail.putExtra("id", contentId);
                v.getContext().startActivity(toApparaatDetail);
            }
        });

        deleteDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(ApparaatAanpassenActivity.this)
                        .setTitle("Zeker weten?")
                        .setMessage("Weet je zeker dat je dit apparaat wilt verwijderen?")
                        .setPositiveButton("Verwijderen", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeDevice(contentId);
                                openHomeActivity();
                            }
                        })
                        .setNegativeButton("Annuleren", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });


    }

    private void getIncomingIntent(){
        if(getIntent().hasExtra("id")){
            Integer imageId = getIntent().getIntExtra("id", 1);
            contentId = imageId;
            changeImage(imageId);
        }
    }

    private void UpdateDevice(Integer imageId){
        JSONObject body = new JSONObject();
        try {
            body.put("name", inputEditApparaatNaam.getText().toString());
            body.put("image_url", imageTag);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = VolleySingleton.getInstance(this).updateDevice(imageId.toString(), body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        db.deviceDAO().updateDevice(contentId, imageTag, inputEditApparaatNaam.getText().toString());
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void changeImage(Integer imageId){
        JSONObject body = new JSONObject();
        try {
            body.put("id", imageId);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(((App)getApplication()).isConnected()) {
            Request request = VolleySingleton.getInstance(this).getDevice(imageId.toString(), body, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    ArrayList<String> namen = new ArrayList<>();
                    ArrayList<String> image = new ArrayList<>();
                    ArrayList<Integer> id = new ArrayList<>();
                    ArrayList<String> tijden = new ArrayList<>();

                    try {
                        JSONObject item = (JSONObject) response;
                        namen.add(item.getString("name"));
                        image.add(item.getString("image_url"));
                        id.add(item.getInt("id"));
                        tijden.add("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    inputEditApparaatNaam.setText(namen.get(0));
                    Context context = apparaatImageToevoegen.getContext();
                    int drawAble = context.getResources().getIdentifier(image.get(0), "drawable", context.getPackageName());
                    apparaatImageToevoegen.setImageResource(drawAble);
                    imageTag = image.get(0);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }else{
            ArrayList<String> namen = new ArrayList<>();
            ArrayList<String> image = new ArrayList<>();
            ArrayList<Integer> id = new ArrayList<>();
            ArrayList<Integer> tijden = new ArrayList<>();

            List<Device> list = db.deviceDAO().getDevice(imageId);
            for(Device device : list){
                namen.add(device.name);
                image.add(device.image_url);
                id.add(device.id);
            }

            inputEditApparaatNaam.setText(namen.get(0));
            Context context = apparaatImageToevoegen.getContext();
            int drawAble = context.getResources().getIdentifier(image.get(0), "drawable", context.getPackageName());
            apparaatImageToevoegen.setImageResource(drawAble);
            imageTag = image.get(0);
        }
    }

    public void imageChosen(View view) {
        CircularImageView image = (CircularImageView) view;
        apparaatImageToevoegen.setImageDrawable(image.getDrawable());
        imageTag = (String) image.getTag();
    }

    private void openHomeActivity(){
        Intent toHomeActivity = new Intent(this, HomeActivity.class);
        this.startActivity(toHomeActivity);
    }

    private void removeDevice(Integer deviceId){
        JSONObject body = new JSONObject();
        try {
            body.put("id", deviceId);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(((App)getApplication()).isConnected()) {
            Request request = VolleySingleton.getInstance(this).deleteDevice(deviceId.toString(), body, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
            db.deviceDAO().removeDevice(deviceId);
        } else{
            new AlertDialog.Builder(this)
                    .setTitle("Geen verbinding")
                    .setMessage("Oh nee! Het lijkt erop dat je geen verbinding met het internet hebt.\nMaak verbinding en probeer het dan nog een keer.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }
}