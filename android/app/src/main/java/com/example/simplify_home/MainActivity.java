package com.example.simplify_home;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.pixplicity.easyprefs.library.Prefs;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Intent toLoginScreenIntent = new Intent(this, LoginActivity.class);
        startActivity(toLoginScreenIntent);
    }
}