package com.example.simplify_home.ui.account;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.simplify_home.AccountWijzigenActivity;
import com.example.simplify_home.LoginActivity;
import com.example.simplify_home.R;
import com.example.simplify_home.database.VolleySingleton;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AccountFragment extends Fragment {

    VolleySingleton volleySingleton;
    private AccountViewModel accountViewModel;
    private Button buttonLogout;
    private Button buttonEditAccount;
    private TextView textViewAccountNaam;
    private TextView textViewAccountEmail;

    final private String BASEURL = "http://192.168.0.176:8000";
    final private String LOGOUTURL = "/api/auth/logout";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        View root = inflater.inflate(R.layout.fragment_account, container, false);
        accountViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        buttonLogout = view.findViewById(R.id.buttonLogout);
        textViewAccountNaam = view.findViewById(R.id.textViewAccountNaam);
        textViewAccountEmail = view.findViewById(R.id.textViewAccountEmail);
        buttonEditAccount = view.findViewById(R.id.buttonEditAccount);
        getAccountDetails();


        volleySingleton = VolleySingleton.getInstance(getActivity());

        buttonEditAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toAccountWijzigen = new Intent(getContext(), AccountWijzigenActivity.class);
                startActivity(toAccountWijzigen);
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //logoutNow();
                openLoginActivity();
            }
        });
    }

    private void getAccountDetails(){
        Request request = VolleySingleton.getInstance(this.getContext()).getUser(new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                ArrayList<String> naam = new ArrayList<>();
                ArrayList<String> email = new ArrayList<>();
                try {
                    JSONObject item = (JSONObject) response;
                    naam.add(item.getString("name"));
                    email.add(item.getString("email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                textViewAccountNaam.setText(naam.get(0));
                textViewAccountEmail.setText(email.get(0));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private void openLoginActivity() {
        Prefs.putString("userToken", null);
        Intent toLoginScreenIntent = new Intent(getContext(),LoginActivity.class);
        startActivity(toLoginScreenIntent);
    }

    private void logoutNow() {
        JSONObject body = new JSONObject();
        try {
            body.put("token", Prefs.getString("token", "none"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, BASEURL + LOGOUTURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String token = null;
                        Prefs.putString("userToken", "");
                        openLoginActivity();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = "";

                        if (error.networkResponse.data != null) {
                            try {
                                body = new String(error.networkResponse.data, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        ){
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Authorization", "Bearer " + Prefs.getString("userToken", null));
                return headers;
            }
        };

        Request request = jsonObjectRequest;

        volleySingleton.addToRequestQueue(request);
    }
}