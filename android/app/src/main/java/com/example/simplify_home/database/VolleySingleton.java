package com.example.simplify_home.database;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleySingleton {
    private static VolleySingleton instance;
    private RequestQueue requestQueue;
    private Context context;

    final private String BASEURL = "http://192.168.0.176:8000"; // M: http://192.168.178.9:8000 C: http://192.168.0.176:8000
    final private String LOGINURL = "/api/auth/login";
    final private String SIGNUPURL = "/api/auth/signup";
    final private String GETUSERURL = "/api/auth/getuser";
    final private String PUTUSERURL = "/api/auth/update-user";
    final private String POSTUSERPASSWORDURL = "/api/auth/reset-password";
    final private String LOGOUTURL = "/api/auth/logout";
    final private String ADDDEVICEURL = "/api/auth/add-device";
    final private String GETDEVICESURL = "/api/auth/get-device";
    final private String GETDEVICEURL = "/api/auth/get-device/";
    final private String UPDATEDEVICEURL = "/api/auth/update-device/";
    final private String DELETEDEVICEURL = "/api/auth/delete-device/";
    final private String ADDLISTURL = "/api/auth/add-list";
    final private String GETLISTSURL = "/api/auth/get-lists";
    final private String GETLISTURL = "/api/auth/get-list/";
    final private String UPDATELISTURL = "/api/auth/update-list/";
    final private String DELETELISTURL = "/api/auth/delete-list/";
    final private String UPDATESTATUSITEMURL = "/api/auth/update-statusItem/";
    final private String UPDATECOUNTERURL = "/api/auth/update-device-timer/";



    private VolleySingleton(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized VolleySingleton getInstance(Context context) {
        if(instance == null) {
            instance = new VolleySingleton(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if(requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }

    public JsonObjectRequest getUser(Response.Listener listener, Response.ErrorListener errorListener) {
        return createAuthGetRequest(GETUSERURL, null, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest updateUser(JSONObject body, Response.Listener listener, Response.ErrorListener errorListener) {
        return createAuthPutRequest(PUTUSERURL, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest updateUserPassword(JSONObject body, Response.Listener listener, Response.ErrorListener errorListener) {
        return createAuthPostRequest(POSTUSERPASSWORDURL, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest getDevices(Response.Listener listener, Response.ErrorListener errorListener) {
        return createAuthGetRequest(GETDEVICESURL, null, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest getDevice(String imageId, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener) {
        Log.d("BODYCHECK", "kaas0 " + body);
        return createAuthGetRequest(GETDEVICEURL + imageId, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest updateDevice(String imageId, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener){
        return createAuthPutRequest(UPDATEDEVICEURL + imageId, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest deleteDevice(String imageId, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener){
        return createAuthDeleteRequest(DELETEDEVICEURL + imageId, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest getLists(Response.Listener listener, Response.ErrorListener errorListener) {
        return createAuthGetRequest(GETLISTSURL, null, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest getList(String title, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener) {
        return createAuthGetRequest(GETLISTURL + title, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest updateList(int list_id, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener){
        return createAuthPutRequest(UPDATELISTURL + list_id, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest deleteList(int list_id, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener){
        return createAuthDeleteRequest(DELETELISTURL + list_id, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest updateStatusItem(String product, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener){
        return createAuthPutRequest(UPDATESTATUSITEMURL + product, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    public JsonObjectRequest updateCountDownTimer(String imageId, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener){
        return createAuthPutRequest(UPDATECOUNTERURL + imageId, body, listener, errorListener, Prefs.getString("userToken", null));
    }

    private JsonObjectRequest createAuthPostRequest(String apiUrl, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener, final String authToken) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASEURL + apiUrl, body, listener, errorListener) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Authorization", "Bearer " + authToken);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return jsonObjectRequest;
    }

    private JsonObjectRequest createAuthGetRequest(String apiUrl, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener, final String authToken) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, BASEURL + apiUrl, body, listener, errorListener) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Authorization", "Bearer " + authToken);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return jsonObjectRequest;
    }

    private JsonObjectRequest createAuthPutRequest(String apiUrl, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener, final String authToken) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASEURL + apiUrl, body, listener, errorListener) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + authToken);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return jsonObjectRequest;

    }

    private JsonObjectRequest createAuthDeleteRequest(String apiUrl, JSONObject body, Response.Listener listener, Response.ErrorListener errorListener, final String authToken) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, BASEURL + apiUrl, body, listener, errorListener) {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Authorization", "Bearer " + authToken);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return jsonObjectRequest;
    }

}
